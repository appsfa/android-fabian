package com.example.fabianmorenoislas.distributech;

import java.text.NumberFormat;
import java.util.Locale;

public class Entidad {

    private String tittle;
    private String content;
    private String imgFoto;
    private String brandPhoto;
    private String price;

    public Entidad(String imgFoto, String tittle, String content, String brandPhoto, Double price) {
        this.imgFoto = imgFoto;
        this.tittle = tittle;
        this.content = content;
        this.brandPhoto = brandPhoto;

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        this.price = format.format(price) + " USD";
    }


    public String getImgFoto() {
        return imgFoto;
    }

    public String getTittle() {
        return tittle;
    }

    public String getContent() {
        return content;
    }

    public String getBrandPhoto() {
        return brandPhoto;
    }

    public String getPrice() {
        return price;
    }
}
