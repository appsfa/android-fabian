package com.example.fabianmorenoislas.distributech;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.*;

import com.android.volley.Response;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Adaptador extends BaseAdapter {
    private Context context;
    private ArrayList<Entidad> arrayList;

    public Adaptador(Context context, ArrayList<Entidad> arrayList) {
        this.context = (Context) context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Entidad item = (Entidad) getItem(position);
        String url = "https://www.distributech.com.mx/" + item.getImgFoto();
        String urlBrand = "https://www.distributech.com.mx/" + item.getBrandPhoto();
        convertView = LayoutInflater.from(context).inflate(R.layout.items, null);
        ImageView imgFoto = (ImageView) convertView.findViewById(R.id.img);
        Picasso.get()
                .load((String) url)
                .into(imgFoto);

        ImageView imgBrand = (ImageView) convertView.findViewById(R.id.imgBrand);
        Picasso.get()
                .load((String) urlBrand)
                .into(imgBrand);

        TextView txtTittle = (TextView) convertView.findViewById(R.id.titleProduct);
        TextView txtContent = (TextView) convertView.findViewById(R.id.contentProduct);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);

        txtPrice.setText(item.getPrice());
        txtTittle.setText(item.getTittle());
        txtContent.setText(item.getContent());

        return convertView;
    }
}
