package com.example.fabianmorenoislas.distributech;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.*;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class search extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
        private ListView listView;
        private Adaptador adapter;
        private Adapter adapter2;

        private ArrayList <String> listProducts;
        private ArrayList <Entidad> listEntidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.setTitle('"' + (String) getIntent().getExtras().getString("search") + '"');
        TextView txtSearch = (TextView) findViewById(R.id.txt_search);
        txtSearch.setText((String) getIntent().getExtras().getString("search"));
        Button buttonSearch = (Button) findViewById(R.id.btn_search);
        buttonSearch.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        listView = (ListView) findViewById(R.id.listViewProducts);
        listProducts = new ArrayList<>();
        listEntidad = new ArrayList<>();

        search();

    }



    private void search()
    {
        RequestQueue mRequestQueue;

// Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

// Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

// Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);

// Start the queue
        mRequestQueue.start();

        String url ="https://www.distributech.com.mx/store/results.php?search=" + (String) getIntent().getExtras().getString("search");

// Formulate the request and handle the response.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String url, tittle, content, brand;
                        Double price;
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            if(jsonArray.length() > 0)
                            {
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject jresponse = jsonArray.getJSONObject(i);
                                    //listProducts.add((String) jresponse.getString("idProduct"));
                                    url = (String) jresponse.getString("url");
                                    tittle = (String) jresponse.getString("idProduct");
                                    content = (String) jresponse.getString("description");
                                    brand = (String) jresponse.getString("photoSB");
                                    price = (Double) jresponse.getDouble("price");
                                    listEntidad.add(new Entidad(url, tittle, content, brand, price));
                                    Adaptador adaptadorEntidad = new Adaptador(getApplicationContext(), listEntidad);

                                    listView.setAdapter(adaptadorEntidad);
                                }
                            }

                            else
                            {
                                listProducts.add("NO SE ENCONTRARON PRODUCTOS");
                                ArrayAdapter <String> adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, listProducts);
                                listView.setAdapter(adaptador);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

// Add the request to the RequestQueue.
        mRequestQueue.add(stringRequest);

// ...
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
            case R.id.search:
                LinearLayout toolbarSearch = (LinearLayout) findViewById(R.id.toolbarSearch);
                //noinspection SimplifiableIfStatement
                if(toolbarSearch.getVisibility() == View.GONE)
                {
                    toolbarSearch.setVisibility(View.VISIBLE);
                }

                else
                {
                    toolbarSearch.setVisibility(View.GONE);
                }
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        int id = item.getItemId();

        if (id == R.id.nav_login) {
            intent =  new Intent(search.this, log_in.class);
            //Toast.makeText(getApplicationContext(),"Home", Toast.LENGTH_SHORT).show();
            // Handle the camera action
        } else if (id == R.id.nav_home) {
            intent =  new Intent(search.this, home.class);
            //Toast.makeText(getApplicationContext(),"Iniciar Sesión", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_signup) {
            intent =  new Intent(search.this, sign_up.class);

        } else if (id == R.id.nav_estimate) {
            intent =  new Intent(search.this, catalogue.class);

        } else if (id == R.id.nav_contact) {
            intent =  new Intent(search.this, contact.class);

        } else if (id == R.id.nav_send) {

        }

        startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        TextView search = (TextView) findViewById(R.id.txt_search);
        switch (v.getId())
        {
            case R.id.btn_search:
                Intent intent = new Intent(search.this, search.class);
                intent.putExtra("search", (String) search.getText().toString());
                startActivity(intent);
                break;

            default:
                break;
        }
    }

    private ArrayList<Entidad> getArrayList(String url, String tittle, String content, String urlBrand, Double price)
    {
        ArrayList<Entidad> arrayList = new ArrayList<>();
        arrayList.add(new Entidad(url, tittle, content, urlBrand, price));

        return arrayList;

    }
}
