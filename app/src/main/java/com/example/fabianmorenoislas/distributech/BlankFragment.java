package com.example.fabianmorenoislas.distributech;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class BlankFragment extends Fragment implements View.OnClickListener{
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blank, null, false);

        view.findViewById(R.id.button_fragment_portrait_landscape).setOnClickListener(this);
        view.findViewById(R.id.button_fragment_drawer).setOnClickListener(this);
        view.findViewById(R.id.button_fragment_tabs).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_fragment_portrait_landscape:
                intentTo(home.class);
                break;
            case R.id.button_fragment_drawer:
                intentTo(home.class);
                break;
            case R.id.button_fragment_tabs:
                intentTo(home.class);
                break;
        }
    }

    private void intentTo(Class classy) {
        Intent intent = new Intent(getActivity(), classy);
        getActivity().startActivity(intent);
    }
}
