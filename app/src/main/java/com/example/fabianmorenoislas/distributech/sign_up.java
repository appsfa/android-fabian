package com.example.fabianmorenoislas.distributech;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class sign_up extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

/*        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
            case R.id.search:
                LinearLayout toolbarSearch = (LinearLayout) findViewById(R.id.toolbarSearch);
                //noinspection SimplifiableIfStatement
                if(toolbarSearch.getVisibility() == View.GONE)
                {
                    toolbarSearch.setVisibility(View.VISIBLE);
                }

                else
                {
                    toolbarSearch.setVisibility(View.GONE);
                }
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        int id = item.getItemId();

        if (id == R.id.nav_login) {
            intent =  new Intent(sign_up.this, log_in.class);
            //Toast.makeText(getApplicationContext(),"Home", Toast.LENGTH_SHORT).show();
            // Handle the camera action
        } else if (id == R.id.nav_home) {
            intent =  new Intent(sign_up.this, home.class);
            //Toast.makeText(getApplicationContext(),"Iniciar Sesión", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_signup) {
            intent =  new Intent(sign_up.this, sign_up.class);

        } else if (id == R.id.nav_estimate) {
            intent =  new Intent(sign_up.this, catalogue.class);

        } else if (id == R.id.nav_contact) {
            intent =  new Intent(sign_up.this, contact.class);

        } else if (id == R.id.nav_send) {

        }

        startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        TextView search = (TextView) findViewById(R.id.txt_search);
        switch (v.getId())
        {
            case R.id.btn_search:
                Intent intent = new Intent(sign_up.this, search.class);
                intent.putExtra("search", (String) search.getText().toString());
                startActivity(intent);
                break;

            default:
                break;
        }
    }
}
